# Lists and Lists #

A to-do list app!

### Features ###

* Add tasks quickly
* Use one of the pre-defined target completion dates (such as 1 day or week), or pick any date you want.
* Modify task names later, or add subtasks to track the item in smaller chunks.

### Tech used ###

* Objective-C, Xcode, iPhone simulator and actual device.
* CoreData used to store all task information and maintaining task/sub-task relationships
* Categories used to extend existing class functionality (enhanced NSManagedObject and UIImage)
* TableViews supplied with data utilizing NSFetchedResultsController with sorting and grouping
* Mogenerator for automated entity subclasses

### Note on project status ###
* This project is not being maintained
* Some elements may be visible but non-functioning, such as the "star" for task favorites, or sub-task completion tracking.