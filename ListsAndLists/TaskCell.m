//
//  TaskCell.m
//  ListsAndLists
//
//  Created by logen on 12/20/14.
//  Copyright (c) 2014 logenw. All rights reserved.
//

#import "TaskCell.h"
#import "colorDefinitions.h"
#import "PrettyDate.h"

@implementation TaskCell

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (highlighted) {
        self.containingView.backgroundColor = UI_ColorFromHex(0x7EC0EE, 0.75);
    } else {
        self.containingView.backgroundColor = [UIColor whiteColor];
    }
}

- (void)updateDateLabel:(NSDate *)date isComplete:(BOOL)isComplete {
    PrettyDate *prettyDate = [PrettyDate createWithDate:date];
    if (isComplete) {
        _dateLabel.text = [NSString stringWithFormat:@"Completed: %@", prettyDate.text];
    } else {
        _dateLabel.text = prettyDate.text;
    }
    
    // Set the label color now that it's been determined, overridden if task completed
    if (_task.isCompletedValue) {
        _dateLabel.textColor = [UIColor blackColor];
    } else {
        _dateLabel.textColor = prettyDate.color;
    }
}

- (void)updateViewForTask:(Task *)task {
    _task = task;

    if (_task.isCompletedValue) {
        _taskName.textColor = UI_BLACK_A(0.5);
        _taskName.attributedText = [[NSAttributedString alloc] initWithString:_task.title
                                                                    attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleThick)}];
        [self updateDateLabel:_task.completedTimestamp isComplete:_task.isCompletedValue];
    } else {
        _taskName.textColor = [UIColor blackColor];
        _taskName.text = _task.title;
        [self updateDateLabel:_task.dueDate isComplete:_task.isCompletedValue];
    }
    _completedCheckbox.selected = _task.isCompletedValue;

}

@end
