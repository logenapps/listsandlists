//
//  PrettyDate.h
//  ListsAndLists
//
//  Created by logen on 1/24/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrettyDate : NSObject

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UIColor *color;

+ (id)createWithDate:(NSDate *)date;

@end
