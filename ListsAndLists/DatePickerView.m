//
//  DatePickerView.m
//  ListsAndLists
//
//  Created by logen on 12/23/14.
//  Copyright (c) 2014 logenw. All rights reserved.
//

#import "DatePickerView.h"
#import "colorDefinitions.h"

#define TOP_VIEW_HEIGHT 44
#define PICKER_HEIGHT 216
#define DATEPICKER_VIEW_HEIGHT (TOP_VIEW_HEIGHT + PICKER_HEIGHT)

@implementation DatePickerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
//        One view that cotains the follow elements of fixed height but variable
//           width using autolayout
//          ----------------------------------------------
//          |                                            |
//    44 px | Left              Title             Right  | --> top view
//          |                                            |
//          ----------------------------------------------
//          |                                            |
//          |                                            |
//    216px |   September          5           2014      | --> date picker
//          |                                            |
//          |                                            |
//          ----------------------------------------------
        
        // top view
        CGFloat topViewHeight = TOP_VIEW_HEIGHT;
        CGSize viewSize = self.frame.size;
        UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewSize.width, topViewHeight)];
        topView.layer.masksToBounds = NO;
        topView.layer.shadowOffset = CGSizeMake(0, 1);
        topView.layer.shadowRadius = 1.5;
        topView.layer.shadowOpacity = 0.5;
        topView.backgroundColor= [UIColor whiteColor];
        [topView setUserInteractionEnabled:YES   ];
        
        // title label
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"Due Date";
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont systemFontOfSize:16.0];
        [topView addSubview:_titleLabel];
        
        // remove button
        _leftButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [_leftButton setTitle:@"Remove" forState:UIControlStateNormal];
        _leftButton.titleLabel.font = [UIFont systemFontOfSize:15.0];
        [topView addSubview:_leftButton];
        
        // done button
        _rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [_rightButton setTitle:@"Done" forState:UIControlStateNormal];
        _rightButton.titleLabel.font = [UIFont systemFontOfSize:15.0];
        [topView addSubview:_rightButton];
        
        // date picker
        CGRect pickerFrame = CGRectMake(0, topViewHeight, viewSize.width, viewSize.height - topViewHeight);
        _picker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        [_picker setDatePickerMode:UIDatePickerModeDate];
        _picker.backgroundColor = [UIColor whiteColor];
        [_picker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
        
        // Adding views in preferred order
        [self addSubview:_picker];
        [self addSubview:topView];
        
        // Layout constraints
        NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(_titleLabel, _leftButton, _rightButton);
        NSDictionary *metrics = @{@"margin": @12};

        // title label constraints
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_titleLabel]-|"
                                                                    options:0
                                                                    metrics:0
                                                                      views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_titleLabel]-|"
                                                                     options:0
                                                                     metrics:0
                                                                       views:viewsDictionary]];
        
        // remove button constraints
        _leftButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[_leftButton]"
                                                                     options:0
                                                                     metrics:metrics
                                                                       views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_leftButton]-|"
                                                                     options:0
                                                                     metrics:0
                                                                       views:viewsDictionary]];

        // done button constraints
        _rightButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_rightButton]-margin-|"
                                                                     options:0
                                                                     metrics:metrics
                                                                       views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_rightButton]-|"
                                                                     options:0
                                                                     metrics:0
                                                                       views:viewsDictionary]];
        
    }
    return self;
}

- (id)pickerInsideViewOfSize:(CGSize)superviewSize {
    CGRect frame = CGRectMake(0, superviewSize.height - DATEPICKER_VIEW_HEIGHT, superviewSize.width, DATEPICKER_VIEW_HEIGHT);
    
    return [self initWithFrame:frame];
}

- (void)pickerChanged:(id)sender {
    NSLog(@"DatePicker: %@", [sender date]);
}

@end
