//
//  DetailViewController.h
//  ListsAndLists
//
//  Created by logen on 12/20/14.
//  Copyright (c) 2014 logenw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"
#import "DatePickerView.h"

@interface DetailViewController : UITableViewController <NSFetchedResultsControllerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSDate *defaultDate;
@property (strong, nonatomic) DatePickerView *datePickerView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) Task *task;
@property (strong, nonatomic) UITextField *subtaskTextField;
@property (weak, nonatomic) IBOutlet UILabel *dueDateLabel;
@property (weak, nonatomic) IBOutlet UITextField *taskTextField;
@property (weak, nonatomic) IBOutlet UIView *dueDateView;
@property (weak, nonatomic) IBOutlet UIView *tableviewHeaderView;
@property (weak, nonatomic) IBOutlet UIView *taskTextFieldView;

@end

