//
//  colorDefinitions.h
//  ListsAndLists
//
//  Created by logen on 12/20/14.
//  Copyright (c) 2014 logenw. All rights reserved.
//

#ifndef ListsAndLists_colorDefinitions_h
#define ListsAndLists_colorDefinitions_h

// Color list:
// cloford.com/resources/colours/500col.htm

// UIColor/ccc3 RGB
#define UI_RGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

// Useful for Hex -> UIColor
#define UI_ColorFromHex(rgbValue, a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
                                                    green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
                                                     blue:((float)(rgbValue & 0xFF))/255.0 \
                                                    alpha:1.0]


#define UI_WHITE_A(a) UI_RGB(255, 255, 255, a)
#define UI_BLACK_A(a) UI_RGB(0, 0, 0, a)
#define UI_CRIMSON UI_RGB(220, 20, 60, 1.0)
#define UI_GREEN_4 UI_RGB(0, 139, 0, 1.0)
#define UI_LIGHTGRAY UI_RGB(249, 249, 249, 1.0)
#define UI_LIGHTGRAY_A(a) UI_RGB(249, 249, 249, a)
#define UI_SLATEGRAY_4 UI_RGB(108, 123, 139, 1.0)
#define UI_SLATEGRAY_4_A(a) UI_RGB(108, 123, 139, a)
#define UI_STEELBLUE_3 UI_RGB(79, 148, 205, 1.0)
#define UI_ROYALBLUE_3 UI_RGB(58, 95, 205, 1.0)
#define UI_LIGHTBLUE UI_RGB(173,216,230,1.0)

#define UI_TEST UIColorFromRGB(0xDB7093, 1.0)

#endif
