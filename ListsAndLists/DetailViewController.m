//
//  DetailViewController.m
//  ListsAndLists
//
//  Created by logen on 12/20/14.
//  Copyright (c) 2014 logenw. All rights reserved.
//

#import "DetailViewController.h"
#import "colorDefinitions.h"
#import "SubtaskCell.h"
#import "NSManagedObject+WATK_Extension.h"
#import "SingleCDStack.h"
#import "PrettyDate.h"

#define kCellIdentifier @"SubtaskCell"
const CGFloat cellHeight = 44.0;

@interface DetailViewController ()

@property BOOL datePickerVisible;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    [self updateViewFields];
}

- (void)configureView {
    // Header setup
    _taskTextFieldView.layer.borderWidth = 0.5;
    _taskTextFieldView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _taskTextField.delegate = self;

    // Change color of background when dueDateView is selected
    UIGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dueDateViewSelected:)];
    [_dueDateView addGestureRecognizer:tapRecognizer];
    _dueDateView.layer.borderWidth = 0.5;
    _dueDateView.layer.borderColor = [UIColor lightGrayColor].CGColor;

    // Register the custom TaskCell
    UINib *taskCellNib = [UINib nibWithNibName:kCellIdentifier bundle:nil];
    [self.tableView registerNib:taskCellNib forCellReuseIdentifier:kCellIdentifier];

    // Adding subtasks is part of the footer
    self.tableView.tableFooterView = [self footerView];

    // Initialize pickerDate and picker (offscreen)
    _defaultDate = _task.dueDate;
    _datePickerVisible = NO;
    [self setupDatePicker];
}

- (void)updateViewFields {
    if (_task) {
        _taskTextField.text = _task.title;

        PrettyDate *prettyDate = [PrettyDate createWithDate:_task.dueDate];
        _dueDateLabel.text = prettyDate.text;
        _dueDateLabel.textColor = prettyDate.color;
        
        [_datePickerView.picker setDate:_task.dueDate animated:YES];
    }
}

- (void)dueDateViewSelected:(id)sender {
    [UIView animateWithDuration:0.15
                     animations:^{
                         _dueDateView.backgroundColor = UI_STEELBLUE_3;
                     } completion:^(BOOL finished){
                         _dueDateView.backgroundColor = [UIColor whiteColor];
                     }];
    [self hideKeyboard];
    [self showDatePicker];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)footerView {
    CGFloat xMargin = 8.0;
    CGFloat footerWidth = self.tableView.frame.size.width - 2 * xMargin;
    CGRect footerFrame = CGRectMake(xMargin, 0, footerWidth, cellHeight);
    UIView *footerView = [[UIView alloc] initWithFrame:footerFrame];
    
    // '+' button on left side
    UIImageView *plusSign = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"add-item.png"]];
    plusSign.frame = CGRectMake(0, 4.5, 35.0, 35.0);
    [footerView addSubview:plusSign];
    
    // Text field setup
    CGFloat vMargin = 3.0;
    CGRect taskTextFieldFrame = CGRectMake(plusSign.frame.size.width,
                                           vMargin,
                                           footerFrame.size.width - plusSign.frame.size.width,
                                           footerFrame.size.height - 2 * vMargin);
    _subtaskTextField = [[UITextField alloc] initWithFrame:taskTextFieldFrame];
    _subtaskTextField.layer.cornerRadius = 5.0;
    _subtaskTextField.backgroundColor = UI_BLACK_A(0.05);
    _subtaskTextField.textColor = [UIColor blackColor];
    _subtaskTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Add a subtask..."
                                                                              attributes:@{NSForegroundColorAttributeName: UI_BLACK_A(0.65)}];
    _subtaskTextField.delegate = self;
    [_subtaskTextField setReturnKeyType:UIReturnKeyNext];
    [_subtaskTextField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    [_subtaskTextField setBorderStyle:UITextBorderStyleNone];
    
    // This is used to give inset appearance
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, xMargin, _subtaskTextField.frame.size.height)];
    _subtaskTextField.leftView = leftView;
    _subtaskTextField.leftViewMode = UITextFieldViewModeAlways;
    [footerView addSubview:_subtaskTextField];
    
    return footerView;
}

-(void) viewWillDisappear:(BOOL)animated {
    // this could definitely be done differently. leaving as I'm abandoning.
    [self updateTaskTitle:_taskTextField.text];
    [super viewWillDisappear:animated];
}

- (void)willMoveToParentViewController:(UIViewController *)parent {

}

#pragma mark TableView
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = nil;
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return cellHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (SubtaskCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SubtaskCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(SubtaskCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Subtask *fetchedSubtask = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.subtask = fetchedSubtask;
    cell.subtaskName.delegate = self;
    cell.subtaskName.text = fetchedSubtask.title;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Create fetch request for Event
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Subtask entityInManagedObjectContext:[SingleCDStack getContext]];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *createdSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdTimestamp" ascending:YES];
    NSArray *sortDescriptors = @[createdSortDescriptor];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Only pull results for this particular task
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"task = %@", _task];
    [fetchRequest setPredicate:predicate];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:[SingleCDStack getContext]
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(SubtaskCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}


#pragma mark - textfields
- (void)updateTaskTitle:(NSString *)title {
    [_task updateTitle:title];
    [SingleCDStack saveChanges];
    [self updateViewFields];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // If textfield is the task itself, update info
    if (_taskTextField == textField) {
        [self updateTaskTitle:textField.text];
    }
    // If textfield was the subtext creation field, create a new subtask
    else if (_subtaskTextField == textField) {
        Subtask *subtask = [Subtask createWithTitle:textField.text forTask:_task];
        [SingleCDStack saveChanges];
    
        // Empty the textfield so another can be added
        _subtaskTextField.text = nil;

        NSLog(@"Subtask created: '%@' at %@", subtask.title, subtask.createdTimestamp);
    }
    // If textfield is within an existing SubtaskCell, update the title, modification date
    else if ([textField.superview.superview.superview isKindOfClass:[SubtaskCell class]]) {
        // Create a subtask if none exists, update otherwise
        SubtaskCell *subtaskCell = (SubtaskCell *)textField.superview.superview.superview;
        Subtask *subtask = subtaskCell.subtask;
        [subtask updateTitle:textField.text];
        [SingleCDStack saveChanges];

        NSLog(@"Subtask modified to: '%@' at %@", subtask.title, subtask.createdTimestamp);
    }

    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (_datePickerVisible) {
        [self hideDatePicker];
    }
}

// This hides the keyboard by asking the view to resign first responder status
- (void)hideKeyboard {
    [self.view endEditing:YES];
}

#pragma mark Date Picker
- (void)resetPickerDate {
    [self updateDueDate:_defaultDate];
}

- (void)showDatePicker {
    if (!_datePickerVisible) {
        [UIView animateWithDuration:0.15
                         animations:^{
                             [self.tableView addSubview:_datePickerView];
                             CGRect pickerFrame = _datePickerView.frame;
                             pickerFrame.origin.y = self.tableView.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height - pickerFrame.size.height;
                             _datePickerView.frame = pickerFrame;
                             _datePickerVisible = YES;
                         }];
    }
}

- (void)hideDatePicker {
    if (_datePickerVisible) {
        [UIView animateWithDuration:0.15
                         animations:^{
                             CGRect pickerFrame = _datePickerView.frame;
                             pickerFrame.origin.y = self.tableView.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
                             _datePickerView.frame = pickerFrame;
                             _datePickerVisible = NO;
                         } completion:^(BOOL finished){
                             [_datePickerView removeFromSuperview];
                         }];
    }
}

- (void)updateDueDate:(NSDate *)date {
    [_task setDueDate:date];
    [SingleCDStack saveChanges];
    [self updateViewFields];
}

- (void)dateChanged:(id)sender {
    [self updateDueDate:_datePickerView.picker.date];
}

- (void)setupDatePicker {
    // Positioning
    _datePickerView = [[DatePickerView alloc] pickerInsideViewOfSize:self.tableView.frame.size];
    CGRect pickerFrame = _datePickerView.frame;
    pickerFrame.origin.y = self.tableView.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
    _datePickerView.frame = pickerFrame;
    _datePickerView.layer.zPosition = 1;
    
    // Actions for picker buttons
    [_datePickerView.leftButton setTitle:@"Reset" forState:UIControlStateNormal];
    [_datePickerView.leftButton addTarget:self action:@selector(resetPickerDate) forControlEvents:UIControlEventTouchUpInside];
    [_datePickerView.rightButton setTitle:@"Done" forState:UIControlStateNormal];
    [_datePickerView.rightButton addTarget:self action:@selector(hideDatePicker) forControlEvents:UIControlEventTouchUpInside];
    [_datePickerView.picker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
}

@end
