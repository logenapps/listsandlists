//
//  SubtaskCell.m
//  ListsAndLists
//
//  Created by logen on 1/1/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "SubtaskCell.h"

@implementation SubtaskCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.containingView.layer.borderWidth = 0.5;
    self.containingView.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Override so that selection coloring doesn't happen.
}


@end
