//
//  SubtaskCell.h
//  ListsAndLists
//
//  Created by logen on 1/1/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Subtask.h"

@interface SubtaskCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containingView;
@property (weak, nonatomic) IBOutlet UITextField *subtaskName;
@property (weak, nonatomic) Subtask *subtask;

@end
