//
//  UIImage+logenwImageWithColor.m
//  ListsAndLists
//
//  Created by logen on 12/24/14.
//  Copyright (c) 2014 logenw. All rights reserved.
//

#import "UIImage+logenwImageWithColor.h"

@implementation UIImage (UIImage_logenwImageWithColor)

+ (id)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, false, 0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (color) {
        CGContextSetFillColorWithColor(context, color.CGColor);
    } else {
        CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    }
    
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}


@end
