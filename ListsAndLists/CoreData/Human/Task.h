#import "_Task.h"

@interface Task : _Task {}

+ (id)createWithTitle:(NSString *)title dueDate:(NSDate *)date;
- (void)toggleCompletedStatus;
- (void)updateTitle:(NSString *)title;

@end
