#import "Task.h"
#import "NSManagedObject+WATK_Extension.h"

@interface Task ()

// Private interface goes here.

@end

@implementation Task

+ (id)createWithTitle:(NSString *)title dueDate:(NSDate *)date {
    Task *task = [Task create];
    task.title = title;
    task.createdTimestamp = [NSDate date];
    task.modifiedTimestamp = [NSDate date];
    task.dueDate = date;
    
    return task;
}

- (void)setCompleted:(NSDate *)date {
    [self setCompletedTimestamp:date];
    [self setIsCompletedValue:(date) ? YES : NO];
}

- (void)toggleCompletedStatus {
    if (self.completedTimestamp) {
        // Date already set -- clear it
        [self setCompleted:nil];
    } else {
        // No date set -- add one
        [self setCompleted:[NSDate date]];
    }
}

- (void)updateTitle:(NSString *)title {
    // Prevent the title from being set to nil if it contained a value previously
    if ([title isEqualToString:@""]) {
        self.title = [self.title substringToIndex:1];
    } else {
        self.title = title;
    }
}

@end
