#import "Subtask.h"
#import "Task.h"
#import "NSManagedObject+WATK_Extension.h"

@interface Subtask ()

// Private interface goes here.

@end

@implementation Subtask

+ (id)createWithTitle:(NSString *)title forTask:(Task *)task {
    Subtask *subtask = [Subtask create];
    subtask.title = title;
    subtask.createdTimestamp = [NSDate date];
    subtask.modifiedTimestamp = [NSDate date];
    subtask.task = task;
    
    return subtask;
}

- (void)updateTitle:(NSString *)title {
    self.title = title;
    self.modifiedTimestamp = [NSDate date];
}

@end
