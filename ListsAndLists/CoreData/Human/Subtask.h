#import "_Subtask.h"

@interface Subtask : _Subtask {}

+ (id)createWithTitle:(NSString *)title forTask:(Task *)task;
- (void)updateTitle:(NSString *)title;

@end
