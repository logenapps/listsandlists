// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Subtask.h instead.

@import CoreData;

extern const struct SubtaskAttributes {
	__unsafe_unretained NSString *createdTimestamp;
	__unsafe_unretained NSString *isCompleted;
	__unsafe_unretained NSString *modifiedTimestamp;
	__unsafe_unretained NSString *title;
} SubtaskAttributes;

extern const struct SubtaskRelationships {
	__unsafe_unretained NSString *task;
} SubtaskRelationships;

@class Task;

@interface SubtaskID : NSManagedObjectID {}
@end

@interface _Subtask : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SubtaskID* objectID;

@property (nonatomic, strong) NSDate* createdTimestamp;

//- (BOOL)validateCreatedTimestamp:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isCompleted;

@property (atomic) BOOL isCompletedValue;
- (BOOL)isCompletedValue;
- (void)setIsCompletedValue:(BOOL)value_;

//- (BOOL)validateIsCompleted:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* modifiedTimestamp;

//- (BOOL)validateModifiedTimestamp:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Task *task;

//- (BOOL)validateTask:(id*)value_ error:(NSError**)error_;

@end

@interface _Subtask (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveCreatedTimestamp;
- (void)setPrimitiveCreatedTimestamp:(NSDate*)value;

- (NSNumber*)primitiveIsCompleted;
- (void)setPrimitiveIsCompleted:(NSNumber*)value;

- (BOOL)primitiveIsCompletedValue;
- (void)setPrimitiveIsCompletedValue:(BOOL)value_;

- (NSDate*)primitiveModifiedTimestamp;
- (void)setPrimitiveModifiedTimestamp:(NSDate*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (Task*)primitiveTask;
- (void)setPrimitiveTask:(Task*)value;

@end
