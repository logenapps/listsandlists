// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Task.m instead.

#import "_Task.h"

const struct TaskAttributes TaskAttributes = {
	.completedTimestamp = @"completedTimestamp",
	.createdTimestamp = @"createdTimestamp",
	.dueDate = @"dueDate",
	.isCompleted = @"isCompleted",
	.modifiedTimestamp = @"modifiedTimestamp",
	.title = @"title",
};

const struct TaskRelationships TaskRelationships = {
	.subtasks = @"subtasks",
};

@implementation TaskID
@end

@implementation _Task

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Task" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Task";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Task" inManagedObjectContext:moc_];
}

- (TaskID*)objectID {
	return (TaskID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isCompletedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isCompleted"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic completedTimestamp;

@dynamic createdTimestamp;

@dynamic dueDate;

@dynamic isCompleted;

- (BOOL)isCompletedValue {
	NSNumber *result = [self isCompleted];
	return [result boolValue];
}

- (void)setIsCompletedValue:(BOOL)value_ {
	[self setIsCompleted:@(value_)];
}

- (BOOL)primitiveIsCompletedValue {
	NSNumber *result = [self primitiveIsCompleted];
	return [result boolValue];
}

- (void)setPrimitiveIsCompletedValue:(BOOL)value_ {
	[self setPrimitiveIsCompleted:@(value_)];
}

@dynamic modifiedTimestamp;

@dynamic title;

@dynamic subtasks;

- (NSMutableSet*)subtasksSet {
	[self willAccessValueForKey:@"subtasks"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"subtasks"];

	[self didAccessValueForKey:@"subtasks"];
	return result;
}

@end

