// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Task.h instead.

@import CoreData;

extern const struct TaskAttributes {
	__unsafe_unretained NSString *completedTimestamp;
	__unsafe_unretained NSString *createdTimestamp;
	__unsafe_unretained NSString *dueDate;
	__unsafe_unretained NSString *isCompleted;
	__unsafe_unretained NSString *modifiedTimestamp;
	__unsafe_unretained NSString *title;
} TaskAttributes;

extern const struct TaskRelationships {
	__unsafe_unretained NSString *subtasks;
} TaskRelationships;

@class Subtask;

@interface TaskID : NSManagedObjectID {}
@end

@interface _Task : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TaskID* objectID;

@property (nonatomic, strong) NSDate* completedTimestamp;

//- (BOOL)validateCompletedTimestamp:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* createdTimestamp;

//- (BOOL)validateCreatedTimestamp:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* dueDate;

//- (BOOL)validateDueDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isCompleted;

@property (atomic) BOOL isCompletedValue;
- (BOOL)isCompletedValue;
- (void)setIsCompletedValue:(BOOL)value_;

//- (BOOL)validateIsCompleted:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* modifiedTimestamp;

//- (BOOL)validateModifiedTimestamp:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *subtasks;

- (NSMutableSet*)subtasksSet;

@end

@interface _Task (SubtasksCoreDataGeneratedAccessors)
- (void)addSubtasks:(NSSet*)value_;
- (void)removeSubtasks:(NSSet*)value_;
- (void)addSubtasksObject:(Subtask*)value_;
- (void)removeSubtasksObject:(Subtask*)value_;

@end

@interface _Task (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveCompletedTimestamp;
- (void)setPrimitiveCompletedTimestamp:(NSDate*)value;

- (NSDate*)primitiveCreatedTimestamp;
- (void)setPrimitiveCreatedTimestamp:(NSDate*)value;

- (NSDate*)primitiveDueDate;
- (void)setPrimitiveDueDate:(NSDate*)value;

- (NSNumber*)primitiveIsCompleted;
- (void)setPrimitiveIsCompleted:(NSNumber*)value;

- (BOOL)primitiveIsCompletedValue;
- (void)setPrimitiveIsCompletedValue:(BOOL)value_;

- (NSDate*)primitiveModifiedTimestamp;
- (void)setPrimitiveModifiedTimestamp:(NSDate*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (NSMutableSet*)primitiveSubtasks;
- (void)setPrimitiveSubtasks:(NSMutableSet*)value;

@end
