// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Subtask.m instead.

#import "_Subtask.h"

const struct SubtaskAttributes SubtaskAttributes = {
	.createdTimestamp = @"createdTimestamp",
	.isCompleted = @"isCompleted",
	.modifiedTimestamp = @"modifiedTimestamp",
	.title = @"title",
};

const struct SubtaskRelationships SubtaskRelationships = {
	.task = @"task",
};

@implementation SubtaskID
@end

@implementation _Subtask

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Subtask" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Subtask";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Subtask" inManagedObjectContext:moc_];
}

- (SubtaskID*)objectID {
	return (SubtaskID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isCompletedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isCompleted"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic createdTimestamp;

@dynamic isCompleted;

- (BOOL)isCompletedValue {
	NSNumber *result = [self isCompleted];
	return [result boolValue];
}

- (void)setIsCompletedValue:(BOOL)value_ {
	[self setIsCompleted:@(value_)];
}

- (BOOL)primitiveIsCompletedValue {
	NSNumber *result = [self primitiveIsCompleted];
	return [result boolValue];
}

- (void)setPrimitiveIsCompletedValue:(BOOL)value_ {
	[self setPrimitiveIsCompleted:@(value_)];
}

@dynamic modifiedTimestamp;

@dynamic title;

@dynamic task;

@end

