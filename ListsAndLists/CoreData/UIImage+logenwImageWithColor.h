//
//  UIImage+logenwImageWithColor.h
//  ListsAndLists
//
//  Created by logen on 12/24/14.
//  Copyright (c) 2014 logenw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage_logenwImageWithColor)

+ (id)imageWithColor:(UIColor *)color;

@end
