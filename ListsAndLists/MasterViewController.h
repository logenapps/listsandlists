//
//  MasterViewController.h
//  ListsAndLists
//
//  Created by logen on 12/20/14.
//  Copyright (c) 2014 logenw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "DatePickerView.h"
#import "LWSegmentedControl.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate, UITextFieldDelegate> {
    BOOL _datePickerVisible;
}

@property (strong, nonatomic) UIButton *insertButton;
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) DatePickerView *datePickerView;
@property (strong, nonatomic) UITextField *taskTextField;
@property (strong, nonatomic) LWSegmentedControl *dueSegmentedControl;
@property (strong, nonatomic) UIView *tableHeaderView;

@end

