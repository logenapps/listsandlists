//
//  PrettyDate.m
//  ListsAndLists
//
//  Created by logen on 1/24/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "PrettyDate.h"
#import "colorDefinitions.h"

@implementation PrettyDate

+ (id)createWithDate:(NSDate *)date {
    PrettyDate *prettyDate = [[PrettyDate alloc] init];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *todayComponents = [calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today =  [calendar dateFromComponents:todayComponents];
    NSDateComponents *dueDateComponents = [calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:date];
    NSDate *otherDate =  [calendar dateFromComponents:dueDateComponents];
    
    // Date format and color varies depending on date comparison
    //  NSOrderedSame
    //    - Today
    //  NSOrderedAscending
    //    - Future
    //  NSOrderedDescending
    //    - Past due
    NSDateFormatter *dateFormatter =  [[NSDateFormatter alloc] init];
    NSComparisonResult dateComparison = [today compare:otherDate];
    switch (dateComparison) {
        case NSOrderedSame: {
            // Today
            prettyDate.text = @"Today";
            prettyDate.color = UI_STEELBLUE_3;
            break;
        }
            
        case NSOrderedAscending: {
            // Future
            NSDateComponents *oneDay = [[NSDateComponents alloc] init];
            [oneDay setDay:1];
            NSDate *tomorrow = [calendar dateByAddingComponents:oneDay toDate:today options:0];
            if ([tomorrow isEqualToDate:otherDate]) {
                // Tomorrow
                prettyDate.text = @"Tomorrow";
            } else if (todayComponents.year == dueDateComponents.year) {
                // Future, same year
                [dateFormatter setDateFormat:@"EEE, MMM dd"];
                 prettyDate.text = [dateFormatter stringFromDate:otherDate];
            } else {
                // Future, later year
                [dateFormatter setDateFormat:@"EEE, MMM dd, yyyy"];
                prettyDate.text = [dateFormatter stringFromDate:otherDate];
            }
            
            prettyDate.color = UI_STEELBLUE_3;
            break;
        }
            
        case NSOrderedDescending: {
            // Past due
            [dateFormatter setDateFormat:@"EEE, MMM dd, yyyy"];
            prettyDate.text = [dateFormatter stringFromDate:otherDate];
            prettyDate.color = UI_CRIMSON;
            break;
        }
            
        default:
            break;
    }
    
    return prettyDate;
}
@end
