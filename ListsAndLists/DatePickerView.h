//
//  DatePickerView.h
//  ListsAndLists
//
//  Created by logen on 12/23/14.
//  Copyright (c) 2014 logenw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatePickerView : UIView

@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
@property (strong, nonatomic) IBOutlet UIDatePicker *picker;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

- (id)pickerInsideViewOfSize:(CGSize)superviewSize;

@end
