 //
//  MasterViewController.m
//  ListsAndLists
//
//  Created by logen on 12/20/14.
//  Copyright (c) 2014 logenw. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "NSManagedObject+WATK_Extension.h"
#import "SingleCDStack.h"
#import "Task.h"
#import "colorDefinitions.h"
#import "TaskCell.h"
#import "UIImage+logenwImageWithColor.h"

#define kCellIdentifier @"TaskCell"
const NSUInteger completeHeaderHeight = 30;

typedef NS_ENUM(NSUInteger, TaskListSections) {
    kIncompleteSection,
    kCompleteSection,
    taskListSectionCount
};

typedef NS_ENUM(NSUInteger, RightNavButtons) {
    kEditButton,
    kInsertButton
};

typedef NS_ENUM(NSUInteger, SegmentedControlItems) {
    kDay,
    kWeek,
    kMonth,
    kCalendar
};

@interface MasterViewController ()

@property (strong, nonatomic) UIImage *grayInsertImage;
@property (strong, nonatomic) UIImage *greenInsertImage;

@end

@implementation MasterViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setupNavInsertButton];
    [self updateRightNavButton:kEditButton];
    
    // Register the custom TaskCell
    UINib *taskCellNib = [UINib nibWithNibName:kCellIdentifier bundle:nil];
    [self.tableView registerNib:taskCellNib forCellReuseIdentifier:kCellIdentifier];
    
    // Tableview Header for task creation
    [self setupTableViewHeader];
    [self updateTableViewHeaderState];
    
    // Hide the keyboard and/or date picker on tap
    UIGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
    
    // Initialize pickerDate and picker (offscreen)
    _datePickerVisible = NO;
    [self setupDatePicker];
    [self setDefaultPickerDate];
    
    // Enable multi-select
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    // This is to make touches work well inside the scrollview
    self.tableView.delaysContentTouches = NO;
}

- (void)updateRightNavButton:(NSUInteger)button {
    switch (button) {
        case kEditButton:
            self.navigationItem.rightBarButtonItem = self.editButtonItem;
            [self.editButtonItem setAction:@selector(editAction:)];
            break;
        
        case kInsertButton:
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_insertButton];
            [self updateInsertButton];
            break;
            
        default:
            break;
    }
    
}

- (void)updateInsertButton {
    if ([_taskTextField.text isEqualToString:@""]) {
        [_insertButton setBackgroundImage:_grayInsertImage forState:UIControlStateNormal];
    } else {
        [_insertButton setBackgroundImage:_greenInsertImage forState:UIControlStateNormal];
    }
}

- (void)updateTableViewHeaderState {
    if ([self.tableView isEditing]) {
        self.tableView.tableHeaderView = nil;
    } else {
        self.tableView.tableHeaderView = _tableHeaderView;
    }
}

// When editing mode is enabled
- (IBAction)editAction:(id)sender {
    [self hidePickerAndKeyboard:nil];
    UIBarButtonItem *button = (UIBarButtonItem *)sender;
    if (!self.tableView.editing) {
        [button setTitle:@"Done"];
        [self.tableView setEditing:YES animated:YES];
        
    } else {
        [button setTitle:@"Edit"];
        [self.tableView setEditing:NO animated:YES];
    }
    [self updateTableViewHeaderState];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self hidePickerAndKeyboard:nil];
}

- (void)hidePickerAndKeyboard:(UITapGestureRecognizer *)sender {
    // Hide views only if already visible/in use
    if (_datePickerVisible) {
        [self hideDatePicker];
    }

    [self hideKeyboard];
}

// Hiding the keyboard updates the right nav button to Edit
- (void)hideKeyboard {
    [self.view endEditing:YES];
    [self updateRightNavButton:kEditButton];
}

- (void)tapped:(UITapGestureRecognizer *)sender {
    // If date picker is visible, hide it if the touch wasn't inside that view
    if (_datePickerVisible) {
        CGPoint location = [sender locationInView:_datePickerView];
        if ((location.x < 0) || (location.x > _datePickerView.frame.size.width) ||
            (location.y < 0) || (location.y > _datePickerView.frame.size.height)) {
            [self hideDatePicker];
        }
    }
    if (_taskTextField.isFirstResponder && (sender.view != _taskTextField)) {
        [self hideKeyboard];
    }
}

- (void)setDefaultPickerDate {
    NSDate *defaultDate = [self dateByAddingMonths:0 weeks:0 days:1];
    [_datePickerView.picker setDate:defaultDate animated:YES];
}

- (void)showDatePicker {
    if (!_datePickerVisible) {
        [UIView animateWithDuration:0.15
                         animations:^{
                             [self.tableView addSubview:_datePickerView];
                             CGRect pickerFrame = _datePickerView.frame;
                             pickerFrame.origin.y = self.tableView.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height - pickerFrame.size.height;
                             _datePickerView.frame = pickerFrame;
                             _datePickerVisible = YES;
                         }];
    }
}

- (void)hideDatePicker {
    if (_datePickerVisible) {
        [UIView animateWithDuration:0.15
                         animations:^{
                             CGRect pickerFrame = _datePickerView.frame;
                             pickerFrame.origin.y = self.tableView.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
                             _datePickerView.frame = pickerFrame;
                             _datePickerVisible = NO;
                         } completion:^(BOOL finished){
                             [_datePickerView removeFromSuperview];
                         }];
    }
}

- (void)setupDatePicker {
    // Positioning
    _datePickerView = [[DatePickerView alloc] pickerInsideViewOfSize:self.tableView.frame.size];
    CGRect pickerFrame = _datePickerView.frame;
    pickerFrame.origin.y = self.tableView.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
    _datePickerView.frame = pickerFrame;
    _datePickerView.layer.zPosition = 1;
    
    // Actions for picker buttons
    [_datePickerView.leftButton setTitle:@"Reset" forState:UIControlStateNormal];
    [_datePickerView.leftButton addTarget:self action:@selector(setDefaultPickerDate) forControlEvents:UIControlEventTouchUpInside];
    [_datePickerView.rightButton setTitle:@"Done" forState:UIControlStateNormal];
    [_datePickerView.rightButton addTarget:self action:@selector(hideDatePicker) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupTableViewHeader {
    CGRect tableFrame = self.tableView.frame;
    CGFloat yPadding = 3.0;
    
    // Text field frame info
    CGFloat textFieldXPadding = 8.0;
    CGFloat textFieldWidth = tableFrame.size.width - 2 * textFieldXPadding;
    CGFloat textFieldHeight = 50.0;
    
    // Due date segmented control frame info
    CGFloat dueSegmentedControlHeight = 30.0;
    CGFloat dueSegmentedControlWidth = 0.55 * tableFrame.size.width;
    CGFloat dueSegmentedControlXPadding = (tableFrame.size.width - dueSegmentedControlWidth) / 2;
    
    // Header view setup
    CGFloat headerViewHeight = textFieldHeight + yPadding + dueSegmentedControlHeight + yPadding * 2;

    _tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableFrame.size.width, headerViewHeight)];
    _tableHeaderView.backgroundColor = [UIColor clearColor];

    // Text field setup
    CGRect taskTextFieldFrame = CGRectMake(textFieldXPadding,
                                   yPadding,
                                   textFieldWidth,
                                   textFieldHeight);
    _taskTextField = [[UITextField alloc] initWithFrame:taskTextFieldFrame];
    _taskTextField.layer.cornerRadius = 5.0;
    _taskTextField.backgroundColor = UI_BLACK_A(0.25);
    _taskTextField.textColor = [UIColor whiteColor];
    _taskTextField.returnKeyType = UIReturnKeyNext;
    _taskTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Add an item..."
                                                                           attributes:@{NSForegroundColorAttributeName: UI_WHITE_A(0.70)}];
    [_taskTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _taskTextField.delegate = self;
    _taskTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
    // This is used to give inset appearance
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15.0, _taskTextField.frame.size.height)];
    _taskTextField.leftView = leftView;
    _taskTextField.leftViewMode = UITextFieldViewModeAlways;
    [_tableHeaderView addSubview:_taskTextField];
    
    // Due date selector setup
    UIImage *calendar = [[UIImage imageNamed:@"calendar small.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    NSArray *segmentedControlItems = [[NSArray alloc] initWithObjects:@"1 day", @"1 week", @"1 month", calendar, nil];
    _dueSegmentedControl = [[LWSegmentedControl alloc] initWithItems:segmentedControlItems];
    [_dueSegmentedControl addTarget:self action:@selector(dueSegmentedControlPressed:) forControlEvents:UIControlEventValueChanged];
    CGRect segmentedControlFrame= CGRectMake(dueSegmentedControlXPadding,
                                             textFieldHeight + 2 * yPadding,
                                             dueSegmentedControlWidth,
                                             dueSegmentedControlHeight);
    _dueSegmentedControl.frame = segmentedControlFrame;
    _dueSegmentedControl.selectedSegmentIndex = kWeek;
    // watk todo -- this didn't fix width on 5S
    [_dueSegmentedControl setApportionsSegmentWidthsByContent:YES];
    [_tableHeaderView addSubview:_dueSegmentedControl];
}

- (void) setupNavInsertButton {
    _grayInsertImage = [UIImage imageWithColor:UI_SLATEGRAY_4_A(0.5)];
    _greenInsertImage = [UIImage imageWithColor:UI_ColorFromHex(0x00CD00, 1)];
    
    _insertButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_insertButton setTitle:@"+" forState:UIControlStateNormal];
    [_insertButton setBackgroundImage:_grayInsertImage forState:UIControlStateNormal];
    [_insertButton setBackgroundImage:[UIImage imageWithColor:UI_STEELBLUE_3] forState:UIControlStateHighlighted];
    [_insertButton setFrame:CGRectMake(0,0, 40, 40)];
    [_insertButton.titleLabel setFont:[UIFont systemFontOfSize:35]];
    [_insertButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_insertButton setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 5, 0)];
    [_insertButton.layer setCornerRadius:9];
    [_insertButton setClipsToBounds:YES];
    [_insertButton addTarget:self action:@selector(insertButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertButtonPressed:(UIButton *)sender {
    [self insertNewObject:sender];
}

- (void)dueSegmentedControlPressed:(id)sender {
    switch (_dueSegmentedControl.selectedSegmentIndex) {
        case kCalendar: {
            [self showDatePicker];
            break;
        }
            
        default:
            [self hideDatePicker];
            break;
    }
}

- (NSDate *)dateByAddingMonths:(NSUInteger)months weeks:(NSUInteger)weeks days:(NSUInteger)days {
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:months];
    [dateComponents setDay:(days + 7 * weeks)];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
}

- (NSDate *)dueDateForNewTask {
    NSDate *dueDate;

    switch (_dueSegmentedControl.selectedSegmentIndex) {
        case kDay:
            dueDate = [self dateByAddingMonths:0 weeks:0 days:1];
            break;

        case kWeek:
            dueDate = [self dateByAddingMonths:0 weeks:1 days:0];
            break;
            
        case kMonth:
            dueDate = [self dateByAddingMonths:1 weeks:0 days:0];
            break;
        
        case kCalendar: {
            dueDate = _datePickerView.picker.date;
            break;
        }
            
        default:
            NSLog(@"Error, unreocgnized segment index (%ld)", (long)_dueSegmentedControl.selectedSegmentIndex);
            break;
    }

    return dueDate;
}

- (void)insertNewObject:(id)sender {
    // Do nothing if there's no text
    if ([_taskTextField.text isEqualToString:@""]) {
        return;
    }
    
    // Create new task based on textfield and segmented control
    Task *task = [Task createWithTitle:_taskTextField.text dueDate:[self dueDateForNewTask]];
    [SingleCDStack saveChanges];
    
    // Clear the text field since task has been added
    _taskTextField.text = nil;
    [self updateInsertButton];

    NSLog(@"Created task: '%@'", task.title);
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Task *object = (Task *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setTask:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - textfields

- (BOOL)textFieldShouldReturn:(UITextField *)textField  {
    if (textField == _taskTextField) {
        [self insertNewObject:_taskTextField];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (_datePickerVisible) {
        [self hideDatePicker];
    }
    [self updateRightNavButton:kInsertButton];
}

- (void)textFieldDidChange:(UITextField *)textField {
    [self updateInsertButton];
}

#pragma mark - Table View

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 0;
    switch (section) {
        case kCompleteSection:
            height = completeHeaderHeight;
            break;
            
        default:
            break;
    }
    return height;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView;
    switch (section) {
        case kCompleteSection: {
            CGRect headerViewFrame = CGRectMake(0, 0, self.tableView.frame.size.width, completeHeaderHeight);
            headerView = [[UIView alloc] initWithFrame:headerViewFrame];
            CGRect labelFrame = CGRectMake(headerViewFrame.size.width * 0.25, 5.0, headerViewFrame.size.width * 0.50, 20.0);
            UILabel *label = [[UILabel alloc] initWithFrame:labelFrame];
            label.text = @"Completed items";
            label.textColor = UI_STEELBLUE_3;
            label.textAlignment = NSTextAlignmentCenter;
            [headerView addSubview:label];
            break;
        }

        default:
            headerView = nil;
            break;
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TaskCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hidePickerAndKeyboard:nil];
    if (!tableView.isEditing) {
        [self performSegueWithIdentifier:@"showDetail" sender:self];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
            
        NSError *error = nil;
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)configureCell:(TaskCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Task *fetchedTask = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell updateViewForTask:fetchedTask];
    [cell.completedCheckbox addTarget:self action:@selector(checkboxPressed:) forControlEvents:UIControlEventTouchUpInside];
}

// Each TaskCell has a checkbox which can toggle completed status
- (void)checkboxPressed:(id)sender {
    TaskCell *cell = (TaskCell *)[[[sender superview] superview] superview];
    [cell.task toggleCompletedStatus];
    [SingleCDStack saveChanges];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Create fetch request for Event
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Task entityInManagedObjectContext:[SingleCDStack getContext]];
    [fetchRequest setEntity:entity];

    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *remainingSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueDate" ascending:YES];
    NSSortDescriptor *sectionSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"isCompleted" ascending:YES];
    NSArray *sortDescriptors = @[sectionSortDescriptor, remainingSortDescriptor];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:[SingleCDStack getContext]
                                                                                                  sectionNameKeyPath:@"isCompleted"
                                                                                                           cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	     // Replace this implementation with code to handle the error appropriately.
	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}    

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(TaskCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

/*
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}
 */

@end
