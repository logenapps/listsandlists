//
//  LWSegmentedControl.m
//  ListsAndLists
//
//  Created by logen on 1/24/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "LWSegmentedControl.h"

@implementation LWSegmentedControl {
    NSInteger _current;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    _current = self.selectedSegmentIndex;
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    if (_current == self.selectedSegmentIndex)
        [self sendActionsForControlEvents:UIControlEventValueChanged];
}

@end
